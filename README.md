# CSS for Fermynwoods (2019)

Artwork by [Tobias Zehntner](http://www.tobiaszehntner.art)

A stylesheet for the website [fermynwoods.org](http://www.fermynwoods.org) commissioned by Fermynwoods Contemporary Art.

- Code licensed as Open Source: [APGL-3.0](https://www.gnu.org/licenses/agpl.html)
<!-- - [Documentation](http://www.tobiaszehntner.art/work/css-fermynwoods) -->
